# -*- coding: cp1252 -*-
from window import *

class Controller():
        def __init__(self):
                self.view = Window((self.homeScreen,self.toolScreen,self.infWin))
                self.zelle=Entry(self.view.container,bd=1,bg="blue")
                self.dataIcon = PhotoImage(file = "resources/database.gif")
                self.likeIcon = PhotoImage(file = "resources/like.gif")
                self.testliste = ["Hammer","Bein","Lffel","Computer","Sichel"]
                self.attribute = []
                self.homeScreen()
                self.view.root.mainloop()

        def homeScreen(self):
                '''Eff.: Setzt Container zurck und lsst den Homescreen erscheinen'''
                self.clear()
                self.like = Canvas(self.view.container)
                self.like.create_image(75,75,image=self.likeIcon)
                self.data = Canvas(self.view.container)
                self.data.create_image(75,75,image= self.dataIcon)                
                self.like.grid(row=2,column=0,pady=100)
                self.data.grid(row=2,column= 1)

        def toolScreen(self):
                '''Eff.: Setzt Container zurck und lsst den Toolscreen
                         mit Tabelle erscheinen'''
                self.clear()
                
  

        def infWin(self):
                '''Eff.: Zeigt ein Infofenster mit den Entwicklern'''
                tkMessageBox.showinfo("Info",'''Made by:        Bruce Weber
                        Christoph Klingenberch
                        Linus Linus
                        Kai Rate''')

        def clear(self):
                '''Eff.: Lscht alle Widgest im Container Frame'''
                for wid in self.view.container.winfo_children():
                        wid.destroy()
                


cont = Controller()
