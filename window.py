# -*- coding: cp1252 -*-

from Tkinter import *
import tkMessageBox
from ctypes import windll

class Login():
        def __init__(self):
                self.root = Tk()
                self.root.title("Login")
                self.usernameE = Entry(self.root)
                self.passwordE = Entry(self.root,show = '*')
                self.loginB = Button(self.root,text = "Login")
                self.pw = None
                self.loginWindow()

        def loginWindow(self):
                '''Eff.:Platziert alle Widgets im Loginfenster'''
                titleL = Label(self.root, text = "Login",font = 20)
                userL = Label(self.root, text = "Benutzername")
                pwL = Label(self.root, text = "Passwort")

                titleL.grid(row = 0,column = 0,columnspan = 2)
                userL.grid(row = 1,column = 0)
                pwL.grid(row = 2,column = 0)
                self.usernameE.grid(row = 1,column = 1)
                self.passwordE.grid(row = 2,column = 1)

                self.loginB.grid(row = 3, column = 0,columnspan = 2)

        def get_Login(self):
                data = (self.usernameE.get(),self.passwordE.get())
                return data
                

class Window():
        def __init__(self,callback):
                self.root = Tk()         
                self.ws = self.root.winfo_screenwidth()/2 # width of the screen
                self.hs = self.root.winfo_screenheight()/2 # height of the screen
                self.entrys = []
                
                self.winheight = 720                         # Fenstergröße
                self.winwidth = 1080                          # Fensterbreite
                self.fifthW = self.winwidth/5
                self.fifthH = self.winheight/5
                self.callback = callback                # Callback ist ein Tupel mit den Fuktionen der Knöpfe.
                                                        # Die Reinfolge ist Tool, Mitarbeiter, Kunden, Verträge, Suche, Fragezeichen
                self.bildMarke = PhotoImage(file = "resources/Communism.gif").subsample(6,6)
                self.endeimg = PhotoImage(file = "resources/power1.gif")
                self.homeIcon = PhotoImage(file = "resources/house.gif")
                self.toolIcon = PhotoImage(file = '''resources/tool.gif''')
                self.workIcon = PhotoImage(file = "resources/worker.gif")
                self.contIcon = PhotoImage(file = "resources/vertrag.gif")
                self.kundIcon = PhotoImage(file = "resources/kunde.gif")
                self.dataIcon = PhotoImage(file = "resources/database.gif")
                self.likeIcon = PhotoImage(file = "resources/like.gif")
                
                self.root.overrideredirect(True)
                self.root.after(5,lambda:self.__set_appwindow(self.root))
                
                self.__fenster()

        
        def __fenster(self):
                '''Eff.: '''
                
                self.root.geometry("%dx%d+%d+%d"%(self.winwidth,self.winheight,self.ws-(self.winwidth/2),self.hs-(self.winheight/2)))
                self.root.title("Toll Tools")
                self.root.resizable(False, False)
                self.root.update_idletasks()
                #self.root.overrideredirect(True)
##                self.root.after(10, lambda: self.set_appwindow())
                #               Frames
                #               ======
                topFrame = Frame(self.root,bg = "#FF0000",width = (self.fifthW)*4,height=10)
                                
                marke = Frame(self.root,bg = "#FF0000",width = 100,height =100)
                
                iconMarke = Canvas(marke,width=50,height=50,bg = "#FF3333",bd = 0)
                
                
                iconMarke.create_image(25,25,image=self.bildMarke)
                
                
                ende = Button(self.root,command=self.root.destroy,image = self.endeimg,relief = FLAT)

                self.container = Frame(self.root,height=(self.winheight/7)*6,width=self.fifthW*4)                                         
                
                buttonFrame = Frame(self.root,bg = "#1A1A1A",width = self.fifthW,height= self.winheight)
                uberschrift = Label(self.root,fg="Black",font = ("Trebuchet MS",50,"bold"),text = "Toll Tools")
                #Fenster verschieben
                topFrame.bind("<ButtonPress-1>", self.__startMove)
                topFrame.bind("<ButtonRelease-1>", self.__stopMove)
                topFrame.bind("<B1-Motion>", self.__onMotion)
                #Backgound placement
                topFrame.place(x=self.fifthW*1,y=0)
                marke.place(x=self.fifthW+(self.fifthW/2),y=0)
                iconMarke.place(x=25,y=25)#ist in Marke nicht root 
                buttonFrame.place(x=0,y=0)
                uberschrift.place(x=self.fifthW*2.5,y=10)
                self.container.place(x=self.fifthW+10,y=(self.winheight/7))
                ende.place(x=self.winwidth-45,y=10)
                #               Buttons
                #               =======
                
##                # Home Button
##                self.homeBut = Button(buttonFrame, bg = "#1A1A1A",
##                                 bd=0, command = self.homeScreen)
##                
##                
##                self.homeBut.config(activebackground = "#1A1A1A",activeforeground = "white")
##                def homeHover(event):
##                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus darüber hovert'''
##                        self.homeBut.config(bg="#515151")
##                        
##                def homeGone(event):
##                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus nichtmehr darüber hovert'''
##                        self.homeBut.config(bg="#1A1A1A")
##                self.homeBut.bind("<Enter>",homeHover)
##                self.homeBut.bind("<Leave>",homeGone)
##                
##                self.homeBut.config(text="    Home",font = ("Trebuchet MS",20),fg="#FFFFFF", image = self.homeIcon,compound="left",anchor="w")
                # Tool Button
                self.toolBut = Button(buttonFrame, bg = "#1A1A1A",width = 30,height=3,
                                 bd=0,command = self.callback[0])    #command = self.callback[0]
                self.toolBut.config(activebackground = "#1A1A1A",activeforeground = "white")
                def homeHover(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus darüber hovert'''
                        self.toolBut.config(bg="#515151")                        
                def homeGone(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus nichtmehr darüber hovert'''
                        self.toolBut.config(bg="#1A1A1A")
                self.toolBut.bind("<Enter>",homeHover)
                self.toolBut.bind("<Leave>",homeGone)
                
                self.toolBut.config(text="    Tools",font = ("Trebuchet MS",20),fg="#FFFFFF", image = self.toolIcon,compound="left",anchor="w")
                # Mitarbeiter Button
                self.workBut = Button(buttonFrame, bg = "#1A1A1A",width = 30,height=3,
                                 bd=0,command = self.callback[1])                
                self.workBut.config(activebackground = "#1A1A1A",activeforeground = "white")
                def homeHover(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus darüber hovert'''
                        self.workBut.config(bg="#515151")                        
                def homeGone(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus nichtmehr darüber hovert'''
                        self.workBut.config(bg="#1A1A1A")
                self.workBut.bind("<Enter>",homeHover)
                self.workBut.bind("<Leave>",homeGone)
                
                self.workBut.config(text="    Arbeiter",font = ("Trebuchet MS",20),fg="#FFFFFF", image = self.workIcon,compound="left",anchor="w")
                # Kunde Button
                self.kundBut = Button(buttonFrame, bg = "#1A1A1A",width = 30,height=3,
                                 bd=0,command = self.callback[2])                
                self.kundBut.config(activebackground = "#1A1A1A",activeforeground = "white")
                def homeHover(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus darüber hovert'''
                        self.kundBut.config(bg="#515151")                        
                def homeGone(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus nichtmehr darüber hovert'''
                        self.kundBut.config(bg="#1A1A1A")
                self.kundBut.bind("<Enter>",homeHover)
                self.kundBut.bind("<Leave>",homeGone)
                
                self.kundBut.config(text="    Kunden",font = ("Trebuchet MS",20),fg="#FFFFFF", image = self.kundIcon,compound="left",anchor="w")
                # Verträge Button
                self.contBut = Button(buttonFrame, bg = "#1A1A1A",width = 30,height=3,
                                 bd=0,command = self.callback[3])                
                self.contBut.config(activebackground = "#1A1A1A",activeforeground = "white")
                def homeHover(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus darüber hovert'''
                        self.contBut.config(bg="#515151")                        
                def homeGone(event):
                        '''Eff.: Ändert die Farbe des Knopfes wenn die Maus nichtmehr darüber hovert'''
                        self.contBut.config(bg="#1A1A1A")
                self.contBut.bind("<Enter>",homeHover)
                self.contBut.bind("<Leave>",homeGone)
                
                self.contBut.config(text="    Verträge",font = ("Trebuchet MS",20),fg="#FFFFFF", image = self.contIcon,compound="left",anchor="w")

                # Info Button
                self.infoBut = Button(buttonFrame,bitmap="question",bd = 0,bg="#1A1A1A",fg="#FFFFFF",activebackground = "#1A1A1A",command = self.callback[-1])
                #Button placement
##                self.homeBut.place(x=0,y=self.fifthH+0             ,height= self.winheight/10,width = self.fifthW)
                self.toolBut.place(x=0,y=self.fifthH/2+((self.winheight/8)*1),height= self.winheight/8,width = self.fifthW)
                self.workBut.place(x=0,y=self.fifthH/2+((self.winheight/8)*2),height= self.winheight/8,width = self.fifthW)
                self.kundBut.place(x=0,y=self.fifthH/2+((self.winheight/8)*3),height= self.winheight/8,width = self.fifthW)
                self.contBut.place(x=0,y=self.fifthH/2+((self.winheight/8)*4),height= self.winheight/8,width = self.fifthW)
                self.infoBut.place(x=0,y=self.winheight-30)
        # Fenster verschieben
        def __startMove(self,event):
                self.x = event.x
                self.y = event.y

        def __stopMove(self,event):
                self.x = None
                self.y = None

        def __onMotion(self,event):
                deltax = event.x - self.x
                deltay = event.y- self.y
                x = self.root.winfo_x() + deltax
                y = self.root.winfo_y() + deltay
                self.root.geometry("+%s+%s"%(x,y))
        #######################


##        def homeScreen(self):
##                '''Eff.: Setzt Container zurück und lässt den Homescreen erscheinen'''
##                self.clear()
##                self.like = Canvas(self.container)
##                self.like.create_image(75,75,image=self.likeIcon)
##                self.data = Canvas(self.container)
##                self.data.create_image(75,75,image= self.dataIcon)                
##                self.like.grid(row=2,column=0,pady=100)
##                self.data.grid(row=2,column= 1)

        def toolScreen(self):
                self.clear()
                title = Label(self.container,font = ("Trebuchet MS",25),text = "Tools")
                title.grid(row=0,column=1)

        def workScreen(self):
                self.clear()
                title = Label(self.container,font = ("Trebuchet MS",25),text = "Mitarbeiter")
                title.grid(row=0,column=1)

        def kundScreen(self):
                self.clear()
                title = Label(self.container,font = ("Trebuchet MS",25),text = "Kunden")
                title.grid(row=0,column=1)

        def contScreen(self):
                self.clear()
                title = Label(self.container,font = ("Trebuchet MS",25),text = "Verträge")
                title.grid(row=0,column=1)

                
        def clear(self):
                '''Eff.: Löscht alle Widgest im Container Frame'''
                for wid in self.container.winfo_children():
                        wid.destroy()

        def infWin(self,publisher):
                '''Eff.: Zeigt ein Infofenster mit den Entwicklern'''
                tkMessageBox.showinfo("Info",'''Made by:%s'''%(publisher))

        def createTable(self,zeilen,spalten):
                '''Vor.: zeilen & spalten ist vom Typ interger
                   Eff.: Es kreiert eine Tabelle mit angegebenen Spalten und Zeilen
                         Jede Zelle ist ein Entry'''                
                
                self.entrys = [] #jede neude Liste in der Liste ist eine neue Zeile
                self.searchs = []
                self.labels = []
                zahlerSpal = 1
                zahlerZei = 1
                for n in range(zeilen-1):
                        newList = []
                        self.entrys.append(newList)
                        
                search = 0
                while zahlerSpal < spalten+1:                        
                        zelle = Label(self.container,justify = CENTER)
                        zelle.grid(row=zahlerZei, column = zahlerSpal,padx=1)
                        zahlerZei+=1
                        self.labels.append(zelle)
                        zelle = Entry(self.container, bd = 1,justify = CENTER)
                        zelle.grid(row=zahlerZei, column = zahlerSpal,padx=1)
                        self.searchs.append(zelle)

                        while zahlerZei < zeilen+1:
                                zahlerZei += 1
                                zelle = Entry(self.container, bd = 1,bg = "grey",justify = CENTER)
                                zelle.grid(row=zahlerZei+1, column = zahlerSpal,padx=1)
                                if zahlerZei%2 > 0:
                                        zelle.config(bg = "lightgrey")
                                self.entrys[zahlerZei-3].append(zelle)
                        zahlerZei = 1
                        zahlerSpal += 1
                for b in self.entrys:
                        print b,'\n'
                self.searchBut = Button(self.container,text = "Suchen",command = self.callback[4],width = 10)
                self.searchBut.grid(row=2,column=zahlerSpal)

                self.addBut = Button(self.container,text = "Neuer Eintrag",command = self.callback[5])
                self.addBut.grid(row=0,column =zahlerSpal)
                                

        def __set_appwindow(self, root):#
                '''Eff.:'''
                GWL_EXSTYLE = -20#
                WS_EX_APPWINDOW = 0x00040000#
                WS_EX_TOOLWINDOW = 0x00000080#
                hwnd = windll.user32.GetParent(root.winfo_id())#
                style = windll.user32.GetWindowLongW(hwnd, GWL_EXSTYLE)#
                style = style & ~WS_EX_TOOLWINDOW#
                style = style | WS_EX_APPWINDOW#
                res = windll.user32.SetWindowLongW(hwnd, GWL_EXSTYLE, style)#
                root.wm_withdraw()#
                root.after(10, lambda: root.wm_deiconify())#



class AddRow():
        def __init__(self):
                self.window = Tk()
                self.window.title("Neuer Eintrag")
                self.adders = []
                self.addLabels = []
                zahlerSpal = 1
                while zahlerSpal < spalten+1:                        
                        zelle = Label(self.window,justify = CENTER,text = str(zahlerSpal))
                        zelle.grid(row=1, column = zahlerSpal,padx=1)
                        self.addLabels.append(zelle)
                        
                        zelle = Entry(self.window, bd = 1,justify = CENTER)
                        zelle.grid(row=2, column = zahlerSpal,padx=1)
                        self.adders.append(zelle)
                        zahlerSpal += 1

        def getEntrys(self):
                wert = []
                for entry in self.adders:
                        wert.append(entry.get())
                return wert
                        
                
                        


if __name__ == "__main__":
        b = (1,1,1,1,1,1,1,1,1)
        #a = Window(b)
        c = AddRow()
        c.window.mainloop()
        #a.root.mainloop()
        
                
