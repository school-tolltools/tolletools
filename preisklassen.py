class Preisklasse:
    def __init__(self, classId, price):
        self.classId = classId
        self.price = price

    def getPrice(self):
        return self.price

    def getClassId(self):
        return self.classId

PREISKLASSEN = {
    'A': Preisklasse(1, 1000),
    'B': Preisklasse(2,  700),
    'C': Preisklasse(3,  450),
    'D': Preisklasse(4,  250)
}

def getPreisklasseById(classId):
    return PREISKLASSEN.values()[classId - 1]

def getPreisklasseByName(name):
    return PREISKLASSEN.get(name)
