# -*- coding: cp1252 -*-

import MySQLdb
from datetime import *

class Model:
    def __init__(self):
        mysql_opts = { 
                "host": "10.16.58.1", 
                "user": "schueler", 
                "pass": "ghoschueler", 
                "db": "tolltool" 
        } 
        mysql = MySQLdb.connect(mysql_opts["host"],
                                mysql_opts["user"],
                                mysql_opts["pass"],
                                mysql_opts["db"])
        mysql.apilevel = "2.0" 
        mysql.threadsafety = 2 
        mysql.paramstyle = "format"
        self.cursor = mysql.cursor()

    def execQuery(self, query):
        self.cursor.execute(query)
        self.cursor.execute("COMMIT;")

    def getDesc(self, table):
        self.cursor.execute("desc {};".format(table))
        return self.cursor.fetchall()

    def getAllEntriesFromTable(self, tableName):
        attributeNames = self.getDesc(tableName)
        attribute = []
        for element in attributeNames:
            attribute.append(element[0])

        self.cursor.execute("SELECT * from {};".format(tableName))
        return self.cursor.fetchall()

    def ausgabe(self, attrNames, output):
        result = []
        for element in output:
            for i in range(len(attrNames)):
                result.append(element[i])

        return result

    def getAuftrag(self):
        return self.getAllEntriesFromTable("auftrag")
              
    def getKunden(self):
        return self.getAllEntriesFromTable("kunden")

    def getMitarbeiter(self):
        return self.getAllEntriesFromTable("mitarbeiter")
        
    def getTools(self):
        return self.getAllEntriesFromTable("tools")

    def addCustomer(self, vName, name):
        self.execQuery("INSERT INTO kunden (kuVName, kuName) " +
                       "VALUES ('{}', '{}');".format(vName, name))

    def addMitarbeiter(self, vName, name):
        self.execQuery("INSERT INTO mitarbeiter (miname, minachname) " +
                       "VALUES ('{}', '{}');".format(vName, name))

    def addAuftrag(self, kuNr, miNr, toolId, endtime):
        timeNow = datetime.now()
        query = "INSERT INTO auftrag (kuNr, minr, startzeit, endzeit, toolId) VALUES ({}, {}, '{}', '{}', {});"
        query = query.format(kuNr, miNr, timeNow.strftime("%Y-%m-%d %H:%M:%S"), endtime.strftime("%Y-%m-%d %H:%M:%S"), toolId)
        self.execQuery(query)

    def addTool(self, toolName, preis, count):
        query = "INSERT INTO tools (toolName, preis, anzahl) VALUES ({}, {}, '{}', '{}', {});"
        query = query.format(toolName, preis, anzahl)
        self.execQuery(query)

    def deleteCustomer(self, kuNr):
        self.execQuery("DELETE FROM kunden WHERE kuNr = {}".format(kuNr))

    def deleteTool(self, toolId):
        self.execQuery("DELETE FROM tools WHERE toolId = {}".format(toolId))

    def deleteMitarbeiter(self, miNr):
        self.execQuery("DELETE FROM mitarbeiter WHERE minr = {}".format(miNr))

    def setToolCount(self, toolId, count):
        self.execQuery("UPDATE tools SET anzahl = {} WHERE toolId = {}".format(anzahl, toolId))

if __name__ == "__main__":
    test = Model()
